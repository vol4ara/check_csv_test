import csv
from datetime import datetime


def convert_time(time_string):
    return datetime.strptime(time_string.strip(), '%H:%M')


def main():
    with open('flights.csv', newline='') as f:
        reader = csv.reader(f)
        data = list(reader)

    success_count = {}
    time_to_compare = convert_time('04:00') - convert_time('01:00')
    for index, flight in enumerate(data):
        flight_name = flight[0]
        time_arrive = convert_time(flight[1].strip())
        time_departure = convert_time(flight[2].strip())
        diff = time_departure - time_arrive
        if not success_count.get(flight_name):
            success_count[flight_name] = 0
        else:
            success_count[flight_name] += 1
        if diff >= time_to_compare and success_count[flight_name] < 20:
            data[index][3] = "success"
        else:
            data[index][3] = "failed"
    return data


if __name__ == '__main__':
    data = main()


    def myFunc(el):
        return convert_time(el[1])


    data_new = sorted(data, key=myFunc)
    for i in data_new:
        print(i)
